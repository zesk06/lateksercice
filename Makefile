all: public/exercice_clean.pdf

public/exercice_clean.tex: src/latex/exercice.tex
	sed "/% BEG_REMOVE/,/% END_REMOVE/d" src/latex/exercice.tex > public/exercice_clean.tex

public/exercice_clean.pdf: public/exercice_clean.tex
	pdflatex public/exercice_clean.tex
	mv exercice_clean.pdf public/exercice_clean.pdf
