[![Build Status](https://gitlab.com/zesk06/lateksercice/badges/master/pipeline.svg)](https://gitlab.com/zesk06/lateksercice/-/commits/master)
---

Ce projet permet de publier du LateX dont certaines sections sont enlevées avant publication.
Les pages publiées sont là: [gitlab pages](https://zesk06.gitlab.io/lateksercice/).

Dans le fichier ``.gitlab-ci.yml`` le bloc script permet de faire le nettoyage et la construction du PDF:

```yaml
    - sed "/% BEG_REMOVE/,/% END_REMOVE/d" src/latex/exercice.tex > public/exercice_clean.tex
    - cd public && pdflatex exercice_clean.tex
```

Il faut configurer les droits d'accès projet gitlab pour que les pages soient publiques, mais le source privé.
